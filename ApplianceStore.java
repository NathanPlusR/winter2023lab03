import java.util.Scanner;

public class ApplianceStore
{
	public static void main (String[] args)
	{
		Scanner scan = new Scanner(System.in);
		Blender[] blenders = new Blender[4];
		for (int i = 0; i < blenders.length; i++)
		{
			blenders[i] = new Blender();
			blenders[i].isPluggedIn = false;
			System.out.println("Blender number " + i);
			System.out.println("How many human legs long is it?");
			blenders[i].heightInHumanLegs = scan.nextDouble();
			System.out.println("What is the radius of this blender in human hands?");
			blenders[i].radiusInHumanHands = scan.nextDouble();
			System.out.println();
		}
		System.out.println("Blender " + (blenders.length-1) + " is it plugged in? " + blenders[blenders.length-1].isPluggedIn + "\nhow long is it? " + blenders[blenders.length-1].heightInHumanLegs + "\nWhat is it's radius? " + blenders[blenders.length-1].radiusInHumanHands);
		
		System.out.println("Do you want to plug in and use your blender? 1 for yes or 0 for no");
		int input = scan.nextInt();
		
		if (input == 1)
		{
			blenders[0].plugInBlender();
			
			System.out.println("How many limbs do you want to blend?");
			double limbs = scan.nextDouble();
			blenders[0].startBlending(limbs);
		}
		else
		{
			System.out.println("Damn alright then. Bye.");
		}
		
	}
}
