public class Application
{
	public static void main (String[] args)
	{
		
		MonarchButterfly bestie = new MonarchButterfly();
		MonarchButterfly bro = new MonarchButterfly();
		
		bestie.family = "Nymphalidae";
		bestie.isMale = false;
		bestie.ageInWeeks = 5;
		
		bro.family = "Nymphalidae";
		bro.isMale = true;
		bro.ageInWeeks = 1;
		
		System.out.println( bestie.currentPhase() );
		System.out.println( bro.currentAverageWeightGrams() );
		
		
		MonarchButterfly[] butterfly = new MonarchButterfly[3];
		butterfly[0] = bestie;
		butterfly[1] = bro;
		butterfly[2] = new MonarchButterfly();
			butterfly[2].family = "Nymphalidae";
			butterfly[2].isMale = true;
			butterfly[2].ageInWeeks = 3;
		
		System.out.println(butterfly[1].currentPhase());
		System.out.println(butterfly[2].currentAverageWeightGrams());
	}
}