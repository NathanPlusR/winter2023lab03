public class MonarchButterfly
{
	String family;
	boolean isMale;
	double ageInWeeks;
	
	public double currentAverageWeightGrams()
	{
		double weight;
		if (ageInWeeks <= 1)
		{
			weight = 0.0005;
		}
		else if (ageInWeeks > 1 & ageInWeeks <= 1.7)
		{
			weight = 1.5;
		}
		else if (ageInWeeks < 1.7 & ageInWeeks < 3)
		{
			weight = 1.2;
		}
		else
		{
			weight = 0.4;
		}
		
		return weight;
	}
	
	public String currentPhase()
	{
		String phase;
		if (ageInWeeks <= 1)
		{
			phase = "Egg";
		}
		else if (ageInWeeks > 1 & ageInWeeks <= 1.7)
		{
			phase = "Larva";
		}
		else if (ageInWeeks < 1.7 & ageInWeeks < 3)
		{
			phase = "Pupa";
		}
		else
		{
			phase = "Adult";
		}
		return phase;
	}

}