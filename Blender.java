
public class Blender
{
	boolean isPluggedIn;
	double heightInHumanLegs;
	double radiusInHumanHands;
	
	public double howManyLimbsCanItBlend()
	{
		double h = this.heightInHumanLegs;
		double r = convertHHtoHL(this.radiusInHumanHands);
		
		double volumeInLimbs = 3.14 * r * r * h;
		
		return volumeInLimbs;
	}
	
	public void plugInBlender()
	{
		if (this.isPluggedIn != true)
		{
			this.isPluggedIn = true;
			System.out.println("Congrats! You've plugged in a blender!");
		}
		else
		{
			System.out.println("What are you kidding or something?");
		}
	}
	
	public static double convertHHtoHL(double radiusInHumanHands) //convert human hands (hh) to human legs (hl)
	{
		return (radiusInHumanHands/5);
	}
	
	public void startBlending(double limbsAmount) //commences production of forbidden soup if prerequisites are met.
	{
		
		if (limbsAmount > this.howManyLimbsCanItBlend())
		{
			System.out.println("That's too many limbs! Don't be greedy!");
		}
		else
		{
			if (this.isPluggedIn == true)
			{
				System.out.println("(Insert grinding noise here)! Bzt! There you go! Your [FORBIDDEN SOUP] has been completed! Enjoy!");
			}
			else
			{
				System.out.println("Plug in your blender first!");
			}
		}
		
	}
}